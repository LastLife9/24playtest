using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Action OnFinishSlice;
    public static Action<GameObject> OnStartSliceObject;

    private GameObject _slicedPart;
    private CurveShapeDeformer _deformerPart;
    [SerializeField] private GameObject cutPanel;
    [SerializeField] private Transform knifeParent;

    public float AnimationSpeed = 1f;
    public float StopYValue = 0f;

    private float _startYValue = 0f;
    private float _animationTimer = 0f;
    private float _sliceProgress = 0f;
    private float _sliceValue = 0f;
    private float _startValueProgress = 0f;

    private bool _move = false;
    private bool _canMove = true;
    private bool _isSlicing = false;

    private void OnEnable()
    {
        MeshSplitting.Splitters.Splitter.OnStartSlice += OnStartCutting;
        OnStartSliceObject += OnStartSlicingPart;
    }
    private void OnDisable()
    {
        MeshSplitting.Splitters.Splitter.OnStartSlice -= OnStartCutting;
        OnStartSliceObject -= OnStartSlicingPart;
    }

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        _startYValue = knifeParent.position.y;
    }

    private void Update()
    {
        if (!_canMove) return;
        _move = Input.GetMouseButton(0);

        MoveKnife();
    }

    private void MoveKnife()
    {
        if (_move)
        {
            _animationTimer = Mathf.Clamp01(_animationTimer + AnimationSpeed * Time.deltaTime);

            if (_animationTimer >= 1)
            {
                OnFinishSlice?.Invoke();
            }

            if (_isSlicing)
            {
                _sliceProgress = Mathf.Clamp01(_sliceProgress + (1f + _startValueProgress) * AnimationSpeed * Time.deltaTime);
                if (_sliceProgress > _sliceValue) _sliceValue = _sliceProgress;
                if(_deformerPart != null) _deformerPart.Multiplier = -_sliceValue;
            }
        }
        else
        {
            if (_isSlicing) return;

            _animationTimer = Mathf.Clamp01(_animationTimer - AnimationSpeed * Time.deltaTime);
        }

        knifeParent.position = new Vector3(
                knifeParent.position.x,
                Mathf.Lerp(_startYValue, StopYValue, _animationTimer),
                knifeParent.position.z);
    }

    private IEnumerator ReturnKnife() 
    {
        float time = 0.5f;
        float elapsedTime = 0;

        while (elapsedTime < time)
        {
            _animationTimer = Mathf.Lerp(1f, 0f, (elapsedTime / time));
            _move = false;
            MoveKnife();
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        _canMove = true;
        cutPanel.SetActive(true);
        Line.Instance.EnableMoving();
    }

    private void OnStartCutting()
    {
        _isSlicing = true;
        _startValueProgress = 1f - _animationTimer;

        cutPanel.SetActive(false);
        Line.Instance.DisableMoving();

        OnFinishSlice += OnFinishedSlice;
    }

    private void OnStartSlicingPart(GameObject part)
    {
        _slicedPart = part;
        _deformerPart = _slicedPart.GetComponentInChildren<CurveShapeDeformer>();
    }

    private void OnFinishedSlice()
    {
        _isSlicing = false;
        _canMove = false;
        _sliceProgress = 0f;
        _sliceValue = 0f;
        _startValueProgress = 0f;

        StartCoroutine(ReturnKnife());
        if(_slicedPart != null) _slicedPart.GetComponent<Rigidbody>().isKinematic = false;

        OnFinishSlice -= OnFinishedSlice;
    }
}
