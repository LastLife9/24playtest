using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : Singleton<Line>
{
    public static Action OnObjectRemove { get; set; }

    public Vector3 MoveAxis;
    public float MinForceSpeed = 1f;
    public float MaxForceSpeed = 1f;
    public float MaxDistance = 5f;

    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private GameObject[] _objectsToSpawn;

    private float _speed;
    private float _distancePassed = 0f;
    private Transform _currentObject;
    private Coroutine _removeCoroutine;
    private bool _moveObjects = true;

    private void OnEnable()
    {
        OnObjectRemove += OnRemoveObject;
    }

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        SpawnObject();
    }

    private void Update()
    {
        if (!_moveObjects) return;
        if (_currentObject == null) return;

        _currentObject.position += MoveAxis * _speed * Time.deltaTime;
        _distancePassed += _speed * Time.deltaTime;

        if (_distancePassed >= MaxDistance)
        {
            if (_removeCoroutine == null)
                _removeCoroutine = StartCoroutine(RemoveObject());
        }
    }


    public void SpawnObject()
    {
        GameObject objToSpawn = _objectsToSpawn[UnityEngine.Random.Range(0, _objectsToSpawn.Length)];
        _currentObject = Instantiate(objToSpawn, _spawnPoint.position, _spawnPoint.rotation).transform;
        _speed = MinForceSpeed;
        EnableMoving();
    }

    private IEnumerator RemoveObject()
    {
        OnObjectRemove?.Invoke();

        float timeToDestroy = 1f;
        float boostMultiplier = 1.4f;
        float elapsedTime = 0;

        while (elapsedTime < timeToDestroy)
        {
            _speed = Mathf.Lerp(MinForceSpeed * boostMultiplier, MaxForceSpeed, (elapsedTime / timeToDestroy));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        Destroy(_currentObject.gameObject);

        _currentObject = null;
        _distancePassed = 0f;

        SpawnObject();

        _removeCoroutine = null;
    }

    public void EnableMoving()
    {
        _moveObjects = true;
    }
    public void DisableMoving()
    {
        _moveObjects = false;
    }
    public bool IsMoving()
    {
        return _moveObjects;
    }
    public void OnRemoveObject()
    {
        Debug.Log("Remove from the line");
    }
}
